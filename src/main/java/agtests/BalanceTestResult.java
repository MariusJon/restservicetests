package agtests;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModelProperty;
import org.springframework.validation.annotation.Validated;

import java.util.Objects;
import java.util.Stack;

/**
 * BalanceTestResult
 */
@Validated
public class BalanceTestResult   {
  @JsonProperty("input")
  private String input = null;

  @JsonProperty("isBalanced")
  private Boolean isBalanced = null;

  public BalanceTestResult input(String input) {
    this.input = input;
    return this;
  }

  /**
   * Get input
   * @return input
  **/
  @ApiModelProperty(example = "[(]", value = "")


  public String getInput() {
    return input;
  }

  public void setInput(String input) {
    this.input = input;
  }

  public BalanceTestResult isBalanced(Boolean isBalanced) {
    this.isBalanced = isBalanced;
    return this;
  }

  public BalanceTestResult (String input)
  {
     this.input = input;
     this.isBalanced  = checkBalance(input);
  }

  private boolean checkBalance(String str)
  {
    Stack<Character> stack = new Stack<Character>();

    char chr;
    char closebracket;
    for(int i=0; i < str.length(); i++) {
      chr = str.charAt(i);

      if(chr == '(' || chr == '{' || chr == '[')
      {
        stack.push(chr);
        continue;
      }

      //No open brackets yet...
      if (stack.isEmpty())
      {
        if(chr == ')' || chr == '}' || chr == ']')
        {
          //first bracket is a close bracket, so brackets cannot be balanced...
          return false;
        }
        else
        {
          //move on to the next char
          continue;
        }

      }

      switch(chr)
      {
        case ')':
          if (stack.pop() != '(')
          {
            return false;
          }
          break;
        case '}':
          if (stack.pop() != '{')
          {
            return false;
          }
          break;
        case ']':
          if (stack.pop() != '[')
          {
            return false;
          }
          break;
      }
    }
    return stack.empty();
  }

  /**
   * Get isBalanced
   * @return isBalanced
  **/
  @ApiModelProperty(example = "false", value = "")


  public Boolean isIsBalanced() {
    return isBalanced;
  }

  public void setIsBalanced(Boolean isBalanced) {
    this.isBalanced = isBalanced;
  }


  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    BalanceTestResult balanceTestResult = (BalanceTestResult) o;
    return Objects.equals(this.input, balanceTestResult.input) &&
        Objects.equals(this.isBalanced, balanceTestResult.isBalanced);
  }

  @Override
  public int hashCode() {
    return Objects.hash(input, isBalanced);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class BalanceTestResult {\n");

    sb.append("    input: ").append(toIndentedString(input)).append("\n");
    sb.append("    isBalanced: ").append(toIndentedString(isBalanced)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

