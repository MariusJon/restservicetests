package agtests;

import java.util.List;
import java.util.ArrayList;
import com.fasterxml.jackson.databind.ObjectMapper;
import io.swagger.annotations.*;
import model.ToDoItemValidationError;
import model.ToDoItemValidationErrorDetails;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Controller
public class TasksApiController implements TasksApi {

    private static final Logger log = LoggerFactory.getLogger(TasksApiController.class);

    private final ObjectMapper objectMapper;

    private final HttpServletRequest request;

    @org.springframework.beans.factory.annotation.Autowired
    public TasksApiController(ObjectMapper objectMapper, HttpServletRequest request) {
        this.objectMapper = objectMapper;
        this.request = request;
    }

    public ResponseEntity<?> tasksValidateBracketsGet(@NotNull @Size(min=1,max=50) @ApiParam(value = "Input string (max length 50)", required = true) @Valid @RequestParam(value = "input", required = true) String input) {
        String accept = request.getHeader("Accept");
        if (accept != null && accept.contains("application/json")) {
            if (input.length() > 0 && input.length()<50) {
                try {
                    BalanceTestResult balanceTestResult = new BalanceTestResult(input);

                    return new ResponseEntity<BalanceTestResult>(balanceTestResult, HttpStatus.OK);
                } catch (Exception e) {
                    log.error("Couldn't serialize response for content type application/json", e);
                    return new ResponseEntity<BalanceTestResult>(HttpStatus.INTERNAL_SERVER_ERROR);
                }
            }

            //Input length is greater than 50, return a Validation Error msg....
            ToDoItemValidationError validationError = new ToDoItemValidationError();
            List<ToDoItemValidationErrorDetails> validationErrorDetailsList = new ArrayList<>();
            validationErrorDetailsList.add(new ToDoItemValidationErrorDetails("params","input","Must be between 1 and 50 chars long", input));

            validationError.setName("ValidationError");

            validationError.setDetails(validationErrorDetailsList);

            return new ResponseEntity<ToDoItemValidationError>(validationError, HttpStatus.BAD_REQUEST);
        }

        return new ResponseEntity<BalanceTestResult>(HttpStatus.NOT_IMPLEMENTED);
    }
}
