package agtests;

import com.fasterxml.jackson.databind.ObjectMapper;
import io.swagger.annotations.*;
import model.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.beans.factory.annotation.Autowired;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.io.IOException;
import java.math.BigDecimal;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;

@javax.annotation.Generated(value = "io.swagger.codegen.languages.SpringCodegen", date = "2018-08-15T10:11:27.962+10:00")

@Controller
public class TodoApiController implements TodoApi {

    private static final Logger log = LoggerFactory.getLogger(TodoApiController.class);

    private final ObjectMapper objectMapper;

    private final HttpServletRequest request;

    @Autowired
    private ToDoRepository toDoRepository;
    private DateFormat df = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");

    public TodoApiController(ObjectMapper objectMapper, HttpServletRequest request) {
        this.objectMapper = objectMapper;
        this.request = request;
    }

    public ResponseEntity<?> todoIdGet(@ApiParam(value = "id",required=true) @PathVariable("id") BigDecimal id) {
        String accept = request.getHeader("Accept");
        if (accept != null && accept.contains("application/json")) {
            try {
                Optional<ToDoItem> toDoItem = toDoRepository.findById(id);

                if (toDoItem.isPresent())
                {
                    return new ResponseEntity<ToDoItem>(toDoItem.get(), HttpStatus.OK);
                }
                else
                {
                    ToDoItemNotFoundError err = new ToDoItemNotFoundError();
                    ArrayList<ToDoItemNotFoundErrorDetails> errDetailsList= new ArrayList<>();
                    errDetailsList.add(new ToDoItemNotFoundErrorDetails("Item with " + id.toString() + " not found"));
                    err.setDetails(errDetailsList);
                    err.setName("NotFoundError");
                    return new ResponseEntity<ToDoItemNotFoundError>(err, HttpStatus.NOT_FOUND);
                }
            } catch (Exception e) {
                log.error("Couldn't serialize response for content type application/json", e);
                return new ResponseEntity<ToDoItem>(HttpStatus.INTERNAL_SERVER_ERROR);
            }
        }

        return new ResponseEntity<ToDoItem>(HttpStatus.NOT_IMPLEMENTED);
    }

    public ResponseEntity<?> todoIdPatch(@ApiParam(value = "id",required=true) @PathVariable("id") BigDecimal id, @ApiParam(value = "" ,required=true )  @Valid @RequestBody ToDoItemUpdateRequest body) {
        String accept = request.getHeader("Accept");
        if (accept != null && accept.contains("application/json")) {
            try {
                Optional<ToDoItem> toDoItem = toDoRepository.findById(id);

                if (toDoItem.isPresent())
                {
                    // Check text length:
                    if (body.getText() != null)
                    {
                        if (body.getText().length() == 0 || body.getText().length()>=50) {
                            //Input length is greater than 50, return a Validation Error msg....
                            ToDoItemValidationError validationError = new ToDoItemValidationError();
                            List<ToDoItemValidationErrorDetails> validationErrorDetailsList = new ArrayList<>();
                            validationErrorDetailsList.add(new ToDoItemValidationErrorDetails("params","input","Must be between 1 and 50 chars long", body.getText()));

                            validationError.setName("ValidationError");

                            validationError.setDetails(validationErrorDetailsList);

                            return new ResponseEntity<ToDoItemValidationError>(validationError, HttpStatus.BAD_REQUEST);
                        }
                    }
                    try {

                        ToDoItem chgToDoItem = toDoItem.get();
                        if (body.getText() != null)
                        {
                            chgToDoItem.setText(body.getText());
                        }
                        if (body.isIsCompleted() != null)
                        {
                            chgToDoItem.setIsCompleted(body.isIsCompleted());
                        }
                        toDoRepository.save(chgToDoItem);
                        return new ResponseEntity<ToDoItem>(chgToDoItem, HttpStatus.OK);
                    } catch (Exception e) {
                        log.error("Couldn't serialize response for content type application/json", e);
                        return new ResponseEntity<ToDoItem>(HttpStatus.INTERNAL_SERVER_ERROR);
                    }

                }
                else
                {
                    ToDoItemNotFoundError err = new ToDoItemNotFoundError();
                    ArrayList<ToDoItemNotFoundErrorDetails> errDetailsList= new ArrayList<>();
                    errDetailsList.add(new ToDoItemNotFoundErrorDetails("Item with " + id.toString() + " not found"));
                    err.setDetails(errDetailsList);
                    err.setName("NotFoundError");
                    return new ResponseEntity<ToDoItemNotFoundError>(err, HttpStatus.NOT_FOUND);
                }
            } catch (Exception e) {
                log.error("Couldn't serialize response for content type application/json", e);
                return new ResponseEntity<ToDoItem>(HttpStatus.INTERNAL_SERVER_ERROR);
            }
        }

        return new ResponseEntity<ToDoItem>(HttpStatus.NOT_IMPLEMENTED);
    }

    public ResponseEntity<?> todoPost(@ApiParam(value = "" ,required=true )  @Valid @RequestBody ToDoItemAddRequest body) {
        String accept = request.getHeader("Accept");
        if (accept != null && accept.contains("application/json")) {
            if (body.getText().length() > 0 && body.getText().length()<=50) {
                try {

                    ToDoItem newToDoItem = new ToDoItem();
                    newToDoItem.setText(body.getText());
                    newToDoItem.setIsCompleted(false); //Freshly created item is not completed yet.
                    newToDoItem.setCreatedAt(df.format(new Date()));
                    toDoRepository.save(newToDoItem);
                    return new ResponseEntity<ToDoItem>(newToDoItem, HttpStatus.OK);
                } catch (Exception e) {
                    log.error("Couldn't serialize response for content type application/json", e);
                    return new ResponseEntity<ToDoItem>(HttpStatus.INTERNAL_SERVER_ERROR);
                }
            }
            //Input length is greater than 50, return a Validation Error msg....
            ToDoItemValidationError validationError = new ToDoItemValidationError();
            List<ToDoItemValidationErrorDetails> validationErrorDetailsList = new ArrayList<>();
            validationErrorDetailsList.add(new ToDoItemValidationErrorDetails("params","input","Must be between 1 and 50 chars long", body.getText()));

            validationError.setName("ValidationError");

            validationError.setDetails(validationErrorDetailsList);

            return new ResponseEntity<ToDoItemValidationError>(validationError, HttpStatus.BAD_REQUEST);
        }

        return new ResponseEntity<ToDoItem>(HttpStatus.NOT_IMPLEMENTED);
    }

}
