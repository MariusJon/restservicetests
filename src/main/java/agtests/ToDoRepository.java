package agtests;

import org.springframework.data.repository.CrudRepository;

import java.math.BigDecimal;

public interface ToDoRepository extends CrudRepository<ToDoItem, BigDecimal>
{

}
