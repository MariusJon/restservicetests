package agtests;

public class ValidationException extends Exception {
    private int code;
    public ValidationException (int code, String msg) {
        super(msg);
        this.code = code;
    }
}
