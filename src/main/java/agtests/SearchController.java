package agtests;

import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.DeleteMapping;

@RestController
@RequestMapping("/")
public class SearchController {

    @GetMapping
    public String getindex() {
        return "Greetings from Spring Boot Getter!";
    }

    @PostMapping
    public String postindex() {
        return "Greetings from Spring Boot Poster!";
    }

    @DeleteMapping
    public String delindex() {
        return "Greetings from Spring Boot Deleter!";
    }

}